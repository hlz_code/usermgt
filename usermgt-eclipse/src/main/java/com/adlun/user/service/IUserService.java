package com.adlun.user.service;

import java.util.List;

import com.adlun.user.bean.User;
import com.alibaba.fastjson.JSONObject;

public interface IUserService{
	
	/**
	 * 查询所有用户
	 * @return
	 */
	public List<User> getUsers();

	/**
	 * 根据用户名查找用户
	 * @param username
	 * @return
	 */
	public List<User> getUserByUserName(String username);

	/**
	 * 新增用户
	 * @param user
	 * @return
	 */
	public JSONObject addUser(User user) ;

	
	/**
	 * 更新用户
	 * @param user
	 * @return
	 */
	public JSONObject updateUser(User user);

	
	/**
	 * 删除用户
	 * @param userid
	 * @return
	 */
	public JSONObject deleteUser(String userid);
}