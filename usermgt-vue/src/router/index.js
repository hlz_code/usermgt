import Vue from 'vue'
import Router from 'vue-router'
import About from '../components/About'
import Home from '../components/Home'
import AddUser from '../components/AddUser'
import EditUser from '../components/EditUser'

Vue.use(Router)

export default new Router({
  routes: [
    {path:"/",component:Home},
    {path:"/adduser",component:AddUser},
    {path:"/edituser",component:EditUser},
    {path:"/about",component:About}

  ]
})
