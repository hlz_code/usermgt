package org.boot.consumer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;

@Service
public class ConsumerService {
	
	private static Logger logger = LoggerFactory.getLogger(ConsumerService.class);

	public String getUsers() {
		RestTemplate template = new RestTemplate();
		User[] users = template.getForObject("http://127.0.0.1:8989/usermgt/users", User[].class);
		logger.info("消费者");
		return JSON.toJSONString(users);
	}

}
