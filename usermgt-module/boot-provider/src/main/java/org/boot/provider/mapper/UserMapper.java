package org.boot.provider.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.boot.provider.bean.User;


@Mapper
public interface UserMapper {
	public List<User> getUsers();

	public List<User> getUserByUserName(String username);

	public void addUser(User user);

	public void updateUser(User user);

	public void deleteUser(String userid);
}
