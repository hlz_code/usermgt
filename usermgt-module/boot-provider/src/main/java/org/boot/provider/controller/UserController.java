package org.boot.provider.controller;

import java.util.List;

import org.boot.provider.bean.User;
import org.boot.provider.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

@RestController
@RequestMapping("/usermgt")
@CrossOrigin
public class UserController {
	
	@Autowired
	private IUserService userService;
	
	@CrossOrigin
	@RequestMapping(value= {"/users"},method=RequestMethod.GET)
	public ResponseEntity<List<User>> getUsers() {
		List<User> list = userService.getUsers();
		return new ResponseEntity<List<User>>(list, HttpStatus.OK);
	}
	
	
	@CrossOrigin
	@RequestMapping(value= {"/user/{username}"},method=RequestMethod.GET)
	public ResponseEntity<List<User>> getUserByUserName(@PathVariable("username") String username) {
		List<User> list = userService.getUserByUserName(username);
		return new ResponseEntity<List<User>>(list, HttpStatus.OK);
	}
	
	
	@CrossOrigin
	@RequestMapping(value= {"/user"},method=RequestMethod.POST)
	public ResponseEntity<JSONObject> addUser(@RequestBody User user) {
		JSONObject result = userService.addUser(user);
		return new ResponseEntity<JSONObject>(result,HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value= {"/user"},method=RequestMethod.PUT)
	public ResponseEntity<JSONObject> updateUser(@RequestBody User user) {
		JSONObject result = userService.updateUser(user);
		return new ResponseEntity<JSONObject>(result,HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value= {"/user/{userid}"},method=RequestMethod.DELETE)
	public ResponseEntity<JSONObject> deleteUser(@PathVariable("userid") String userid) {
		JSONObject result = userService.deleteUser(userid);
		return new ResponseEntity<JSONObject>(result,HttpStatus.OK);
	}
	

}
