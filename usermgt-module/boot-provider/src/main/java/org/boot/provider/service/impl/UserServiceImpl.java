package org.boot.provider.service.impl;

import java.util.List;
import java.util.UUID;

import org.boot.provider.bean.User;
import org.boot.provider.mapper.UserMapper;
import org.boot.provider.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.alibaba.fastjson.JSONObject;
@Service
public class UserServiceImpl implements IUserService{

	@Autowired
	private UserMapper userMapper;
	
	@Override
	public List<User> getUsers(){
		return userMapper.getUsers();
	}
	
	@Override
	public List<User> getUserByUserName(String username) {
		return userMapper.getUserByUserName(username);
	}
	
	@Override
	public JSONObject addUser(User user) {
		JSONObject resultJson = new JSONObject();
		user.setId(UUID.randomUUID().toString());
		userMapper.addUser(user);
		resultJson.put("result", "success");
		return resultJson;
	}
	
	@Override
	public JSONObject updateUser(User user) {
		JSONObject resultJson = new JSONObject();
		userMapper.updateUser(user);
		resultJson.put("result", "success");
		return resultJson;
	}
	
	@Override
	public JSONObject deleteUser(String userid) {
		JSONObject resultJson = new JSONObject();
		userMapper.deleteUser(userid);
		resultJson.put("result", "success");
		return resultJson;
	}

}
