package org.boot.provider.controller;

import org.boot.consumer.service.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsumerController {
	
	@Autowired
	private ConsumerService service;
	
	@RequestMapping(value = {"/consumer"},method = RequestMethod.GET)
	public ResponseEntity<String> getUsers() {
		String users = service.getUsers();
		return new ResponseEntity<String>(users,HttpStatus.OK);
	}

}
