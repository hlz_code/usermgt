DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` varchar(36) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `school` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `introduction` varchar(1000) DEFAULT NULL COMMENT '介绍',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`email`,`school`,`phone`,`address`,`introduction`) values ('1','墨墨','momo@163.com','北京大学','1879432423','北京','我叫墨墨，大名大墨墨，小名小墨墨，装逼名晔墨墨，当然也可以叫我老墨墨，但是拜托不要叫我默默，我没那么默默无闻，更别叫我陌陌，从来不免费给约炮神器打广告。');
insert  into `user`(`id`,`username`,`email`,`school`,`phone`,`address`,`introduction`) values ('12321','李四','lisi@aliyun.com','武汉大学','1888888888','武汉','承认现实，实事求是地对待自我，命运掌握在自己手中，勤奋铸就成功，我相信自己会有一个美好的未来。');


