package com.primeton.eos.usermgt.util;



public class Constant {
	/**
	 * 跨域资源共享
	 */
	public static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
	/**
	 * 跨域资源共享允许值
	 */
	public static final String CORS_ACCEPT = "*";
	
	
}
