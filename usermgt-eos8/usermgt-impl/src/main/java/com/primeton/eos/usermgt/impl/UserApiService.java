package com.primeton.eos.usermgt.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.eos.foundation.database.DatabaseExt;
import com.primeton.eos.usermgt.api.User;
import com.primeton.eos.usermgt.api.UserApi;
import com.primeton.eos.usermgt.util.Constant;




@Controller
public class UserApiService implements UserApi{
	
	

	@Override
	public ResponseEntity<Map[]> queryUsers() throws Throwable {
		HashMap<String,String> map = new HashMap<>();
		Object[] obj =  DatabaseExt.queryByNamedSql("default", "com.primeton.eos.usermgt.impl.user.queryUsers", map);
		Map[] data = new HashMap[obj.length];
		for(int i = 0;i< obj.length;i++){
			if(obj[i] instanceof HashMap){
				data[i] = (Map) obj[i];
			}
		}
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add(Constant.ACCESS_CONTROL_ALLOW_ORIGIN, Constant.CORS_ACCEPT);
		return new ResponseEntity<Map[]>(data, headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Map[]> queryUserByName(@PathVariable(name = "username") String username) throws Throwable {
		HashMap<String,String> map = new HashMap<>();
		Object[] obj =  DatabaseExt.queryByNamedSql("default", "com.primeton.eos.usermgt.impl.user.queryUserByName", username);
		Map[] data = new HashMap[obj.length];
		for(int i = 0;i< obj.length;i++){
			if(obj[i] instanceof HashMap){
				data[i] = (Map) obj[i];
			}
		}
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add(Constant.ACCESS_CONTROL_ALLOW_ORIGIN, Constant.CORS_ACCEPT);
		return new ResponseEntity<Map[]>(data,headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> addUser(@RequestBody User user) throws Throwable {
		String result = "{\"result\":\"success\"}";
		user.setId(UUID.randomUUID().toString());
		DatabaseExt.executeNamedSql("default", "com.primeton.eos.usermgt.impl.user.addUser", user);
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add(Constant.ACCESS_CONTROL_ALLOW_ORIGIN, Constant.CORS_ACCEPT);
		return new ResponseEntity<String>(result,headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> updateUser(@RequestBody User user) throws Throwable {
		String result = "{\"result\":\"success\"}";
		DatabaseExt.executeNamedSql("default", "com.primeton.eos.usermgt.impl.user.updateUser", user);
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add(Constant.ACCESS_CONTROL_ALLOW_ORIGIN, Constant.CORS_ACCEPT);
		return new ResponseEntity<String>(result,headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> delUser(@PathVariable(name = "userid") String userid) throws Throwable {
		String result = "{\"result\":\"success\"}";
		DatabaseExt.executeNamedSql("default", "com.primeton.eos.usermgt.impl.user.deleteUser", userid);
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add(Constant.ACCESS_CONTROL_ALLOW_ORIGIN, Constant.CORS_ACCEPT);
 		return new ResponseEntity<String>(result,headers, HttpStatus.OK);
	}

}
