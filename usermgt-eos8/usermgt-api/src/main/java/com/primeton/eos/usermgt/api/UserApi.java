package com.primeton.eos.usermgt.api;

import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.primeton.anyware.tarest.core.api.annotation.TarestOperation;
import com.primeton.anyware.tarest.core.api.annotation.TarestService;

@CrossOrigin
@TarestService(group = "com.primeton.eos.back.api", name = "user", version = "1.0.0", appId = "back")
public interface UserApi {
	
     @TarestOperation(name = "queryUsers")
     @RequestMapping(
			value = "/users",
			produces = { "application/json" },
			method = RequestMethod.GET)
     @CrossOrigin
	 ResponseEntity<Map[]> queryUsers() throws Throwable;
	 
	 
	 @TarestOperation(name = "queryUserByName")
     @RequestMapping(
			value = "/user/{username}",
			produces = { "application/json" },
			method = RequestMethod.GET)
	 @CrossOrigin
	 ResponseEntity<Map[]> queryUserByName(@PathVariable(name = "username") String username) throws Throwable;
	 
	 @TarestOperation(name = "addUser")
     @RequestMapping(
			value = "/user",
			consumes = { "application/json" },
			produces = { "application/json" },
			method = {RequestMethod.POST})
	 @CrossOrigin
	 ResponseEntity<String> addUser(@RequestBody User user) throws Throwable;
	 
	 @TarestOperation(name = "updateUser")
     @RequestMapping(
			value = "/user",
			produces = { "application/json" },
			method = RequestMethod.PUT)
	 @CrossOrigin
	 ResponseEntity<String> updateUser(@RequestBody User user) throws Throwable;
	 
	 
	 @TarestOperation(name = "delUser")
     @RequestMapping(
			value = "/user/{userid}",
			produces = { "application/json" },
			method = RequestMethod.DELETE)
	 @CrossOrigin
	 ResponseEntity<String> delUser(@PathVariable("userid") String userid) throws Throwable;
	 
	 
   
    }
